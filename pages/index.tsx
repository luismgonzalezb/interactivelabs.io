import React from "react";
import { NextPage } from "next";
import { Section, Title } from "rbx";
import MainHero from "../components/MainHero";
import HeroLayout from "../layouts/HeroLayout";

const Home: NextPage = () => (
  <HeroLayout Hero={MainHero}>
    <Section>
      <Title as="h3">Landing Page</Title>
    </Section>
  </HeroLayout>
);

export default Home;
