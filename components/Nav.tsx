import React from "react";
import Link from "next/link";
import { Navbar } from "rbx";

const links = [
  { href: "/", label: "Home", key: "" },
  { href: "/about", label: "About", key: "" },
  { href: "/contact", label: "Contact", key: "" }
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

const Nav = () => (
  <Navbar fixed="top" color="primary">
    <Navbar.Brand>
      <Navbar.Item href="#">IL</Navbar.Item>
      <Navbar.Burger />
    </Navbar.Brand>
    <Navbar.Menu>
      <Navbar.Segment align="start">
        {links.map(item => (
          <Link key={item.key} href={item.href}>
            <Navbar.Item>{item.label}</Navbar.Item>
          </Link>
        ))}
      </Navbar.Segment>
    </Navbar.Menu>
  </Navbar>
);

export default Nav;
