import { Content, Footer } from "rbx";

import "bulma-pro/css/bulma.css";

const SiteFooter = () => (
  <Footer>
    <Content textAlign="centered">
      <p>&copy; Interactive Labs. All rights reserved.</p>
    </Content>
  </Footer>
);

export default SiteFooter;
