import React from "react";
import { Container, Section, Title, Hero } from "rbx";

const MainHero = () => (
  <Section backgroundColor="primary">
    <Hero>
      <Hero.Body>
        <Container>
          <Title as="h1" align="center" color="white">
            Welcome to Next!
          </Title>
        </Container>
      </Hero.Body>
    </Hero>
  </Section>
);

export default MainHero;
