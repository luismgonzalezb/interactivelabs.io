import { Generic, Container, Content } from "rbx";
import Nav from "../components/Nav";
import SiteFooter from "../components/SiteFooter";

import "bulma-pro/css/bulma.css";

const HeroLayout = ({ children, Hero }) => (
  <Generic>
    <Nav />
    <Hero />
    <Container>
      <Content>{children}</Content>
    </Container>
    <SiteFooter />
  </Generic>
);

export default HeroLayout;
