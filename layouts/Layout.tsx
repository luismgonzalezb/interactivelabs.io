import { Generic, Container, Content } from "rbx";
import Nav from "../components/Nav";
import SiteFooter from "../components/SiteFooter";

import "bulma-pro/css/bulma.css";

const Layout = ({ children }) => (
  <Generic>
    <Nav />
    <Container>
      <Content>{children}</Content>
    </Container>
    <SiteFooter />
  </Generic>
);

export default Layout;
